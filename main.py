# https://gitlab.com/sea1100010/knight_and_bestie
import random

global wins
wins = 0

knight = {"health":10, "power":10}
bestie = {"health":1, "power":1}

print("Состояние рыцаря: ", knight["health"], knight["power"])

def fight() -> bool:
    while True:
        knight["health"] -= bestie["power"]
        bestie["health"] -= knight["power"]
        if knight["health"] <= 0:
            result = False # погиб рыцарь
            break
        if bestie["health"] <= 0:
            result = True # погибло чудовище
            break
    return result

def life():

    while True:
        event = random.randint(1, 3)  # генерация события, 1 - яблоко, 2 - меч, 3 - чудовище
        # событие - яблоко
        if event == 1:
            apple = random.randint(5, 10)  # генерация жизни яблока
            knight["health"] += apple
            print("Рыцарь съел яблоко, жизнь увеличилась на ", apple, " и составляет ", knight["health"])
            continue
        # событие - меч
        if event == 2:
            sword = random.randint(5, 10)  # генерация силы атаки меча
            print("Рыцарь встретил меч с силой атаки ", sword)

            action = input("Выберите действие (взять - 1, пройти мимо - 2): ")
            while not (action == "1" or action == "2"):
                print("некорректный ввод")
                action = input("Выберите действие (взять - 1, пройти мимо - 2): ")

            if action == "1":
                knight["power"] = sword
                print("Рыцарь взял меч")
            if action == "2":
                print("Рыцарь прошел мимо меча")
            continue

        # событие - встреча чудоыища
        if event == 3:
            bestie["health"] = random.randint(5, 10)  # генерация жизни чудовища
            bestie["power"] = random.randint(5, 10)  # генерация силы атаки меча чудовища
            print("Рыцарь встретил чудовище с жизнью ", bestie["health"], "и с силой атаки ", bestie["power"])
            action = input("Выберите действие (сражаться - 1, убежать - 2): ")
            while not (action == "1" or action == "2"):
                print("некорректный ввод")
                action = input("Выберите действие (сражаться - 1, убежать - 2): ")

        if action == "1":
            if fight():
                print("Чудовище повержено!")
                global wins
                wins += 1
                if wins == 10:
                    print("Победа!")
                    break
            else:
                print("Рыцарь погиб. Game over.")
                break

        if action == "2":
            print("Рыцарь убежал от чудовища")

        continue
    return 0

if __name__ == '__main__':
    life()
